###############################################################################
##define global variables
### absolute path to the folder which contains paired-end raw reads files and sample description file 
DATA_PATH=$1
if [[ -z "$DATA_PATH" ]]
then
	EDNA_PATH=/media/superdisk/edna
    DATA_PATH="$EDNA_PATH"/donnees/rhone_all
fi
### absolute path to the folder which contains reference database files
BDR_PATH=$2
if [[ -z "$BDR_PATH" ]]
then
	EDNA_PATH=/media/superdisk/edna
    BDR_PATH="$EDNA_PATH"/donnees/rhone_all
fi
###############################################################################
## list fastq files
for i in `ls "$DATA_PATH"/*_R1.fastq.gz`;
do
basename $i | cut -d "." -f 1 | sed 's/_R1//g'
done > liste_fq
##liste des fichiers dat
for i in `ls "$DATA_PATH"/*dat`;
do
echo $i
done > liste_dat
## list of fastq files and corresponding sample description files
paste liste_fq liste_dat > liste_fq_dat
rm liste_fq liste_dat
## writing bash script with all commands for each pair of fastq and corresponding .dat files
while IFS= read -r var
do
echo "bash pipeline_single.sh "$var" "$DATA_PATH" "$BDR_PATH
done < liste_fq_dat > fq_dat_cmd.sh
###############################################################################
## run in parallel each command
parallel < fq_dat_cmd.sh
